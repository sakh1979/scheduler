# Spring Scheduler Example

## Prerequisite
Please make sure you have following software installed in your computer:

* java 8
* mvn 3

## Build Command
```shell
mvn clean install
```

## Executing
```shell
java -jar target/scheduler-test-0.1.0.jar
```

## RESTful Web Service
You can hit the REST server via:
```
http://localhost:8080/greeting
```

to pass in a variable to the REST server use:
```
http://localhost:8080/greeting?name=World
```

in above case, the variable `name` will be set to "World".

## Scheduler
Spring Framework uses cron syntax to schedule tasks.
